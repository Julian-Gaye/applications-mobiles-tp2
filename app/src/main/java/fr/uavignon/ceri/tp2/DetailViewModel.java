package fr.uavignon.ceri.tp2;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {

    private BookRepository repository;
    private MutableLiveData<Book> selectedBook;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
        selectedBook = repository.getSelectedBook();
    }

    MutableLiveData<Book> getSelectedBook() {
        return selectedBook;
    }

    void findBook(long id) {
        repository.getBook(id);
    }

    public void insertOrUpdateBook(Book book, long choix) {
        if(choix == -1)
            repository.insertBook(book);
        else
            repository.updateBook(book);
    }
}
