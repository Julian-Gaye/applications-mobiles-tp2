package fr.uavignon.ceri.tp2;

import android.app.Activity;
import android.inputmethodservice.Keyboard;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    private DetailViewModel viewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        viewModel.findBook(args.getBookNum());
        MutableLiveData<Book> book = viewModel.getSelectedBook();

        textTitle = view.findViewById(R.id.nameBook);
        textAuthors = view.findViewById(R.id.editAuthors);
        textYear = view.findViewById(R.id.editYear);
        textGenres = view.findViewById(R.id.editGenres);
        textPublisher = view.findViewById(R.id.editPublisher);

        if(args.getBookNum() == -1) { //Si on ajoute un livre, on change le bouton "Mettre à jour" en "Ajouter"
            Button button = view.findViewById(R.id.buttonUpdate);
            button.setText("Ajouter");
        }
        else { //Sinon on écrit les infos du livre dans les champs
            textTitle.setText(book.getValue().getTitle());
            textAuthors.setText(book.getValue().getAuthors());
            textYear.setText(book.getValue().getYear());
            textGenres.setText(book.getValue().getGenres());
            textPublisher.setText(book.getValue().getPublisher());
        }

        listenerSetup();
    }

    private void listenerSetup() {
        Button updateButton = getView().findViewById(R.id.buttonUpdate);
        Button backButton = getView().findViewById(R.id.buttonBack);
        Snackbar snackbar = Snackbar.make(getView(), "Le titre et l'auteur doivent être indiqués", 5000);


        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = textTitle.getText().toString();
                String authors = textAuthors.getText().toString();
                String year = textYear.getText().toString();
                String genres = textGenres.getText().toString();
                String publisher = textPublisher.getText().toString();
                Book book = new Book(title, authors, year, genres, publisher);
                if(title.length() == 0 || authors.length() == 0) {
                    snackbar.show();
                }
                else {
                    snackbar.dismiss();
                    DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
                    if (args.getBookNum() != -1)
                        book.setId(args.getBookNum());
                    viewModel.insertOrUpdateBook(book, args.getBookNum());
                    if (args.getBookNum() == -1) {
                        NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this).navigate(R.id.action_DetailFragment_to_ListFragment);
                        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0); //Fermer le clavier quand on revient
                    }
                    else {
                        Snackbar snackbarUpdate = Snackbar.make(getView(), "Le livre a été mis à jour", 1000);
                        snackbarUpdate.show();
                    }
                }
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0); //Fermer le clavier quand on revient au premeir écran
                snackbar.dismiss();
            }
        });
    }
}